---
title: Acerta API


language_tabs:
  - shell
  - python

toc_footers:
  - <a href='#'>Sign Up for a Developer Key</a>
  - <a href='https://github.com/tripit/slate'>Documentation Powered by Slate</a>

includes:
  - errors

search: true
---

# Introduction

The Acerta API is used for uploading sensor data from vehicles and vehicle sub-systems. Acerta's machine learning platform will run an analysis in real-time and provide results highlighting the anomalous regions.

# Authentication

> Registering new user:

```shell
curl -k -u 12345: -i -X GET https://zf.acerta.ca/login/info

```

> Make sure to replace `12345` with your API key.

Acerta customers will be given a unique token based on their user name.

Acerta expects the API key to be included in all API requests to the server

`Authorization: 12345`

<aside class="notice">
You must replace <code>12345</code> with your personal API key.
</aside>

# Upload Unit

```shell
curl "http://example.com/api/upload"
  -H "Authorization: 12345"
```

> The above command returns JSON structured like this:

```json
{
   'success':True,
   204
}
```

This endpoint allows upload of gearbox data.

### HTTP Request

`GET http://example.com/api/upload`

### Query Parameters

Parameter | Description
--------- | -----------
BOM | The BOM number of the gearbox
Station | The station number of the gearbox

# Get Unit

```shell
curl "http://example.com/api/gearbox?status=inspect&startdate=20170513&enddate=20170613"
  -H "Authorization: 12345"
```

> The above command returns JSON structured like this:

```json
[
  {
      "trace_id": "2017051106",
      "trace_date": "2017-04-24",
      "defect_type": "Gear tooth fracture detected",
      "result": "fail",
      "inspection_status": "fixed"
  },
  {
      "trace_id": "2017051802",
      "trace_date": "2017-03-18",
      "defect_type": "N/A",
      "result": "pass",
      "inspection_status": "passed"
  }
]
```

This endpoint allows retrival of gearbox data.

### HTTP Request

`GET http://example.com/api/gearbox`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
status | all | The status of the gearbox. <br>Statuses include <code>inspect, needs_review, fixed, ignored, pass</code>
startdate | -30d | The start date required for the selection of gearbox
enddate | Today | The end date required for the selection of gearbox

## Get a Specific Unit

```shell
curl "http://example.com/api/gearbox/2017042406"
  -H "Authorization: 12345"
```

> The above command returns JSON structured like this:

```json
{
      "trace_id": "2017042406",
      "trace_date": "2017-03-18",
      "defect_type": "N/A",
      "result": "pass",
      "inspection_status": "passed",
      "notes": "The gearbox shows no sign of damage",
      "viz": [
          {
            "img1": "s3.compute.amazonaws.com/2017031802/resultgraph1.png"
          },
          {
            "img2": "s3.compute.amazonaws.com/2017031802/resultgraph2.png"
          }
      ]
  }
```

This endpoint retrieves a specific unit.

### HTTP Request

`GET http://example.com/gearbox/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the unit to retrieve

# Update Unit

```python
updateGearbox(
{
'trace_id':'2017042406',
'notes': 'UPDATE: Gearbox has been fixed'
})
```

> The above command returns JSON structured like this:

```json
{
   'success':True,
   204
}
```

This endpoint allows update of unit data.

### HTTP Request

`PUT http://example.com/api/gearbox`

### Query Parameters

Parameter | Description
--------- | -----------
ID | The ID of the unit to retrieve
notes | Modification of engineering notes

# Reporting

```python
getTestAggregate(
{
'startdate':'20170513',
'enddate': '20170613'
})
```

> The above command returns JSON structured like this:

```json
{
  "total_passed": 78,
  "total_failed": 5,
  "total_tested": 83
}
```

This endpoint allows retrival of reporting statistics that can be used in dashboards.

### HTTP Request

`GET http://example.com/api/reporting`

### Query Parameters

Parameter | Description
--------- | -----------
startdate | -30d
enddate | Today

## Daily

```python
getDailyAggregate(
{
'startdate':'20170513',
'enddate': '20170613'
})
```

> The above command returns JSON structured like this:

```json
[
  {
    "trace_date": "2017-04-24",
    "total_passed": 5,
    "total_failed": 2,
    "total_tested": 7
  }, {
    "trace_date": "2017-04-25"
    "total_passed": 4,
    "total_failed": 0,
    "total_tested": 5
  }, {
    "trace_date": "2017-04-26"
    "total_passed": 2,
    "total_failed": 1,
    "total_tested": 5
  }
]
```

This endpoint shows the daily breakdown of passing and failing units.

### HTTP Request

`GET http://example.com/api/reporting`

### Query Parameters

Parameter | Description
--------- | -----------
startdate | -30d
enddate | Today

## Monthly

```python
getMonthlyAggregate(
{
'startdate':'20160513',
'enddate': '20170613'
})
```

> The above command returns JSON structured like this:

```json
[
  {
    "trace_date": "2017-01-01",
    "total_passed": 50,
    "total_failed": 7,
    "total_tested": 57
  }, {
    "trace_date": "2017-02-01"
    "total_passed": 55,
    "total_failed": 8,
    "total_tested": 63
  }, {
    "trace_date": "2017-03-01"
    "total_passed": 49,
    "total_failed": 4,
    "total_tested": 53
  }
]
```

This endpoint shows the monthly breakdown of passing and failing units.

### HTTP Request

`GET http://example.com/api/reporting`

### Query Parameters

Parameter | Description
--------- | -----------
startdate | -1y
enddate | Today

