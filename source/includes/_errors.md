# Errors

The Acerta API uses the following error codes:


Error Code | Meaning
---------- | -------
400 | Bad Request -- Your request is incorrect
401 | Unauthorized -- Your API key is wrong
403 | Forbidden -- You do not have the correct priviledges to access this content
404 | Not Found -- The specified item could not be found
406 | Not Acceptable -- You requested a format that isn't json
500 | Internal Server Error -- We had a problem with our server. Try again later.
503 | Service Unavailable -- We're temporarily offline for maintenance. Please try again later.
